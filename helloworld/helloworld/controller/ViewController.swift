//
//  ViewController.swift
//  helloworld
//
//  Created by Andrea Wilkins on 2019/10/18.
//  Copyright © 2019 Microsoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{
    
    // variable initialisation
    var inputText: String!
    var sub: String!
    var region: String!
    
    var favouriteItems: [String] = ["Hello, how are you?", "I have laringitis so I can't speak", "My throat is sore", "Sorry, there's a delay"]
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var favouritesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // load subscription information
        sub = "bc90bdfa69444e53b51dbd4be6f88c94"
        region = "uksouth"
        
        //set initial values
        inputText = ""
        textField.delegate = self
        
        //programatic styling
        favouritesButton.backgroundColor = .clear
        favouritesButton.layer.cornerRadius = 20
        favouritesButton.layer.borderWidth = 1
        favouritesButton.layer.borderColor = UIColor(red:0.96, green:0.49, blue:0.49, alpha:1.0).cgColor
        self.hideKeyboardWhenTappedAround()

    }
    
    //button to synthesize text to speech
    @IBAction func translateButton(_ sender: Any) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.synthesisToSpeaker()
        }
        textField.text = ""
    }
    
    //add favourite phrase to favourites page
    @IBAction func favouritesButton(_ sender: Any) {
//        let text = "test"
        saveFavourite(text: inputText)
        textField.text = ""
    }
    
    //navigate to favourites page
    @IBAction func toFavourites(_ sender: Any) {
        performSegue(withIdentifier: "toFavourites", sender: nil)
    }
    
    //reload favourite items on favourites page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is FavouritesController {
            let viewController = segue.destination as? FavouritesController
            viewController?.favouriteItems = self.favouriteItems
        }
    }

    //grab input from text field to use later
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
        let textRange = Range(range, in: text) {
            self.inputText = text.replacingCharacters(in: textRange, with: string)
        }
        return true
    }
    
    //Synthesize text to speech
    func synthesisToSpeaker() {
//        print(inputText)
        var speechConfig: SPXSpeechConfiguration?
        do {
            try speechConfig = SPXSpeechConfiguration(subscription: sub, region: region)
        } catch {
            print("error \(error) happened")
            speechConfig = nil
        }
        let synthesizer = try! SPXSpeechSynthesizer(speechConfig!)
        if inputText.isEmpty {
            return
        }
        let result = try! synthesizer.speakText(inputText)
        if result.reason == SPXResultReason.canceled
        {
            let cancellationDetails = try! SPXSpeechSynthesisCancellationDetails(fromCanceledSynthesisResult: result)
            print("cancelled, detail: \(cancellationDetails.errorDetails!) ")
        }
    }
    
}

//add favourite to array of favourites
extension ViewController {
    func saveFavourite(text: String) {
        let favourite = text
        favouriteItems.append(favourite)
//        print(favouriteItems[0])
    }
}

//dismiss keyboard when tapping anywhere on screen
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
