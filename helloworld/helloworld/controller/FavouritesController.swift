//
//  FavouritesController.swift
//  helloworld
//
//  Created by Andrea Wilkins on 2019/10/22.
//  Copyright © 2019 Microsoft. All rights reserved.
//

import Foundation
import UIKit

class FavouritesController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var sub: String!
    var region: String!
    
    //initialise values
    let identifier = "cell"
    var favouriteItems: [String] = ["Hello, how are you?", "I have laringitis so I'm not allowed to speak", "My throat is sore", "Sorry, there's a delay"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sub = "bc90bdfa69444e53b51dbd4be6f88c94"
        region = "uksouth"
        
        // Do any additional setup after loading the view.
        tableView.reloadData()
//        print(favouriteItems[0])
        tableView.tableFooterView = UIView()
    
    }
    
    //navigate back to main page
    @IBAction func onBackTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //clear favourites from list of favourites
    @IBAction func onClearTap(_ sender: Any) {
        favouriteItems.removeAll()
        tableView.reloadData()
        
        if let viewController = self.presentingViewController as? ViewController {
            DispatchQueue.main.async {
                viewController.favouriteItems.removeAll()
//                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //Synthesize text to speech
        func synthesisToSpeaker(inputText: String) {
    //        print(inputText)
            var speechConfig: SPXSpeechConfiguration?
            do {
                try speechConfig = SPXSpeechConfiguration(subscription: sub, region: region)
            } catch {
                print("error \(error) happened")
                speechConfig = nil
            }
            let synthesizer = try! SPXSpeechSynthesizer(speechConfig!)
            if inputText.isEmpty {
                return
            }
            let result = try! synthesizer.speakText(inputText)
            if result.reason == SPXResultReason.canceled
            {
                let cancellationDetails = try! SPXSpeechSynthesisCancellationDetails(fromCanceledSynthesisResult: result)
                print("cancelled, detail: \(cancellationDetails.errorDetails!) ")
            }
        }
    
}

extension FavouritesController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteItems.count
    }
    
    //diplay favourites in table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let favourite = favouriteItems[indexPath.row]
        cell.textLabel?.text = favourite
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(favouriteItems[indexPath.row])
        
        let inputText = favouriteItems[indexPath.row]
        
        synthesisToSpeaker(inputText: inputText)
    }
}

