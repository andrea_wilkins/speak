# Speak.

### What is Speak?

Speak is an iOS application that synthesizes text into speech using Microsoft Azure's cognitive services. The user can type in anything they would like to say and when they click the speak button, the device will speak the text for them.
They can then also add certain phrases to their favourites. These favourite phrases can be spoken out just by tapping on them, making it quicker and easier for users to speak their most commonly used phrases.

### Who is Speak for?

Speak is for people who have been rendered temporarily mute due to illness or surgery. It helps them communicate with those around them while they wait for their voices to come back.

### Getting Started

* Clone the repository to your local machine.
* Open the repository in Xcode. Make sure you open the helloworld.xcworkspace file.
* If your device asks you to update some Xcode packages, make sure that you update them and restart Xcode.
* Once you have the project open, change the device to an iPhone 8 before running the project.
* Click the play button in the top left hand corner to run the project.

### Tech

Speak was created using:

* [Swift] - For the front-end and business logic of the application
* [Microsoft Azure's Cognitive Services] - for the artificial intelligence part of the application


   [Swift]: <https://swift.org/documentation/>
   [Microsoft Azure's Cognitive Services]: <https://azure.microsoft.com/en-us/services/cognitive-services/>
   
![alt Speak](images/speak.png)

